from EnviratronFileNameParser import EnviratronFileNameParser

# Be sure that EnviratronFileNameParser is on PYTHONPATH
import arrow


def test_hyperspectral_bin():
    fpath = "/iplant/home/enviratron/exp_data/experiments/35/chamber_2/c2_2018_8_10_10_15_21_559/hs_16_2018_8_10_10_31_56_103_f_1112_w_1024_h_56.bin"
    parser = EnviratronFileNameParser(fpath)

    assert parser.type == "hyperspectral"
    assert parser.bands == 56
    assert parser.width == 1024
    assert parser.height == 1112
    assert parser.ordinal == 16

    assert parser.chamber_id == 2

    assert parser.year == 2018
    assert parser.month == 8
    assert parser.day == 10
    assert parser.hour == 10
    assert parser.minute == 31


def test_hyperspectral_bin_datetime():
    fpath = "/iplant/home/enviratron/exp_data/experiments/35/chamber_2/c2_2018_8_10_10_15_21_559/hs_16_2018_8_10_10_31_56_103_f_1112_w_1024_h_56.bin"
    parser = EnviratronFileNameParser(fpath)

    assert parser.year == 2018
    assert parser.month == 8
    assert parser.day == 10
    assert parser.hour == 10
    assert parser.minute == 31

    file_datetime = arrow.get(parser.datetime)
    assert file_datetime.year == 2018
    assert file_datetime.month == 8
    assert file_datetime.day == 10
    assert file_datetime.hour == 10
    assert file_datetime.minute == 31
    assert file_datetime.second == 56

    assert parser.bands == 56
    assert parser.height == 1112
    assert parser.width == 1024


def test_pam_yml_file():
    fpath = "PAM_16_2018_8_10_9_18_35_508.yml"
    parser = EnviratronFileNameParser(fpath)
    assert parser.type == "fluorometer pam"

    fpath = "rgb_6_2018_8_10_8_57_45_507.jpg"
    parser = EnviratronFileNameParser(fpath)
    assert parser.type == "rgb image"

    fpath = "rgb_pose_4_2018_8_10_9_7_48_31.yml"
    parser = EnviratronFileNameParser(fpath)
    assert parser.type == "rgb pose"


def test_thermal_pose_file_type():

    filenames = """/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_11_2018_8_10_9_4_20_565.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_12_2018_8_10_9_3_23_608.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_13_2018_8_10_9_11_15_52.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_14_2018_8_10_9_10_19_433.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_15_2018_8_10_9_13_13_513.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_16_2018_8_10_9_12_12_906.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_1_2018_8_10_9_6_45_574.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_2_2018_8_10_9_5_46_986.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_3_2018_8_10_9_8_50_81.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_4_2018_8_10_9_7_44_145.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_5_2018_8_10_8_58_40_611.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_6_2018_8_10_8_57_43_308.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_10_8_56_49_389/thermo_pose_7_2018_8_10_9_0_34_883.yml
/thermo_pose_8_2018_8_10_8_59_36_796.yml
thermo_pose_9_2018_8_10_9_2_27_498.yml""".split(
        "\n"
    )

    for fname in filenames:
        parser = EnviratronFileNameParser(fname)
        assert parser.type == "thermal pose"

        assert parser.year == 2018
        assert parser.month == 8
        assert parser.day == 10

        if "/chamber_" in fname:
            assert parser.chamber_id == 1
        else:
            assert parser.chamber_id is None

        # Quick and dirty access to the ordinal:
        quick_and_dirty_ordinal = int(fname.split("thermo_pose_")[-1].split("_")[0])
        assert parser.ordinal == quick_and_dirty_ordinal

        assert hasattr(parser, "bands") is False
        assert hasattr(parser, "height") is False
        assert hasattr(parser, "width") is False

    bad_filenames = """/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_15_17_56_56_163/depth_8_2018_8_15_17_59_44_272.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_15_17_56_56_163/depth_9_2018_8_15_18_2_33_971.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_15_17_56_56_163/depth_pose_10_2018_8_15_18_1_37_970.yml
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_15_17_56_56_163/depth_pose_11_2018_8_15_18_4_27_205.yml""".split(
        "\n"
    )

    for fname in bad_filenames:
        parser = EnviratronFileNameParser(fname)
        assert parser.type != "thermal pose"


def test_old_format_hyperspectral_reference_filenames():
    filenames = """c1_2018_8_6_9_21_39_74/1_hs_rd_2018_8_6_9_22_4_10_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/1_hs_rd_2018_8_6_9_22_4_10_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/1_hs_rw_2018_8_6_9_22_4_10.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/1_hs_rw_2018_8_6_9_22_4_10_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/1_hs_rw_2018_8_6_9_22_4_10_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rd_2018_8_6_9_32_33_939.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rd_2018_8_6_9_32_33_939_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rd_2018_8_6_9_32_33_939_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rw_2018_8_6_9_32_33_939.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rw_2018_8_6_9_32_33_939_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_6_9_21_39_74/2_hs_rw_2018_8_6_9_32_33_939_tcp_pose.csv""".split(
        "\n"
    )

    for fname in filenames:

        parser = EnviratronFileNameParser(fname)

        if "/chamber_" in fname:
            assert parser.chamber_id == 1

        if parser.file_extension == "bin":
            assert "hyperspectral reference" in parser.type
        elif parser.file_extension == "csv":
            # TODO: This shouldn't be a single assert!
            assert (
                parser.type == "hyperspectral pose"
                or parser.type == "hyperspectral csv"
            )


def test_new_format_hyperspectral_reference_filenames():

    fname = "/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_1_d_rp_2_2018_8_17_9_52_8_787_tcp_pose.csv"
    parser = EnviratronFileNameParser(fname)

    assert parser.year == 2018
    assert parser.month == 8
    assert parser.day == 17
    assert parser.ordinal == 2

    assert parser.file_extension == "csv"
    assert parser.type == "hyperspectral reference pose"

    filenames = """hsr_m_0_d_rp_0_2018_8_17_9_45_26_509.csv
Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_0_2018_8_17_9_45_26_509_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_0_2018_8_17_9_45_26_509_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_1_2018_8_17_9_32_49_114.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_1_2018_8_17_9_32_49_114_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_1_2018_8_17_9_32_49_114_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_2_2018_8_17_9_40_58_63.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_2_2018_8_17_9_40_58_63_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_d_rp_2_2018_8_17_9_40_58_63_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_0_2018_8_17_9_45_26_509.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_0_2018_8_17_9_45_26_509_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_0_2018_8_17_9_45_26_509_tcp_pose.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_1_2018_8_17_9_32_49_114.csv
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_1_2018_8_17_9_32_49_114_f_30_w_1024_h_56.bin
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/hsr_m_0_w_rp_1_2018_8_17_9_32_49_114_tcp_pose.csv""".split(
        "\n"
    )

    ordinals_from_filenames = (0, 0, 0, 1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1)

    for i in range(0, len(filenames)):
        fname = filenames[i]
        parser = EnviratronFileNameParser(fname)

        assert "hyperspectral" in parser.type

        assert parser.year == 2018
        assert parser.month == 8
        assert parser.day == 17
        assert parser.hour == 9

        assert parser.ordinal == ordinals_from_filenames[i]

        if parser.file_extension == "bin":
            assert parser.width == 1024
            assert parser.height == 30
            assert parser.bands == 56
        else:
            assert hasattr(parser, "width") is False
            assert hasattr(parser, "height") is False
            assert hasattr(parser, "bands") is False


def test_infrared():

    fname = "/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_20_9_32_29_340/ir_7_2018_8_20_9_36_12_341.bin"
    parser = EnviratronFileNameParser(fname)

    assert parser.type == "infrared"
    assert parser.ordinal == 7
    assert parser.year == 2018
    assert parser.month == 8
    assert parser.day == 20
    assert parser.hour == 9
    assert parser.minute == 36
    assert parser.second == 12


def test_jpegs():

    filenames = """/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_10_2018_8_17_9_37_7_938.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_11_2018_8_17_9_39_59_348.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_12_2018_8_17_9_39_1_711.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_13_2018_8_17_9_46_47_837.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_14_2018_8_17_9_45_51_349.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_15_2018_8_17_9_48_47_89.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_16_2018_8_17_9_47_46_143.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_1_2018_8_17_9_42_24_516.jpg
/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_17_9_32_22_144/thermo_2_2018_8_17_9_41_25_585.jpg""".split(
        "\n"
    )

    for fname in filenames:
        parser = EnviratronFileNameParser(fname)

        assert parser.year == 2018
        assert parser.month == 8
        assert parser.file_extension == "jpg"
        assert parser.type == "thermal image"
        assert hasattr(parser, "width")
        assert hasattr(parser, "height")


def test_thermal_bin_file():

    fname = "thermo_64F_7_2018_8_3_9_50_55_465.bin"
    parser = EnviratronFileNameParser(fname)
    assert hasattr(parser, "width") and parser.width is not None

def test_alternate_code_paths():

    fname = "/Volumes/Data/enviratron_imaging/35_clean/chamber_1/c1_2018_8_20_9_32_29_340/ir_7_2018_8_20_9_36_12_341.bin"
    parser = EnviratronFileNameParser(fname)
    parser.ints_list = None
    parser.parse_ordinal()

    assert parser.ordinal == 7

    parser.parse_chamber_id()

    assert parser.chamber_id == 1


    _dict = parser.as_dict()

    assert "chamber_id" in _dict
    assert "ordinal" in _dict
    assert "datetime" in _dict
    assert "year" in _dict
    assert "file_extension" in _dict
